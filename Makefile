NS = registry.gitlab.com/grzes.rap
IMAGE = `basename $$PWD`

build:
	docker build $(CACHE_ARGS) -t $(IMAGE) .

push:
	docker tag $(IMAGE) $(NS)/$(IMAGE)
	docker push $(NS)/$(IMAGE) 

test:
	docker run -d -p 0.0.0.0:8080:8080 --restart unless-stopped --name=$(IMAGE) $(IMAGE)

test-rm:
	docker rm -f $(IMAGE)
