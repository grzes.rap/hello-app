FROM golang:1.12.0-alpine3.9

RUN mkdir /app
ADD hello-app/ /app
WORKDIR /app

RUN go mod download

RUN go build -o main .

EXPOSE 8080/tcp

USER nobody:nobody
CMD ["/app/main"]
